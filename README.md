# Removing_specific_elements

This repository contains a python script (assignment_1.py), which removes specific contours (in this case line segments) based on pixel locations specified by the user.


## Required python packages for this repository
* ```numpy```
* ```opencv-python (cv2)```
* ```argsparse (default python library)``` 

## Running the code:

Ensure your are inside the repository folder in the command line. 

Run with (default: without arguments):
~~~
python assignment_1.py
~~~ 

In order to pass pixel locations or image name as arguments check the below examples.


Example:

(To remove the horizontal line at the bottom in sample image a1_1.PNG)
~~~
python assignment_1.py --indices "146,648" --image-location a1_1.PNG
~~~

(To remove the vertical line on the left side in sample image a1_1.PNG)
~~~
python assignment_1.py --indices "93,90" --image-location a1_1.PNG
~~~

(To remove both lines in sample image)
~~~
python assignment_1.py --indices "146,648,93,90" --image-location a1_1.PNG
~~~

The indices parser argument takes comma separated pixel locations. In the above example, this implies that two lines with pixel locations [146,648] and [93,90] along the line have to be removed. The second parser argument --image-location requires the name and location of image.

After running the program, the output will be displayed in the command line. Press any key in your keyboard to exit the program.

## Python coding convention used
Style: PEP 8

Docstring type: Google docstring

## Report

The main report is available in the Documents folder under Report.pdf, which shows the Object Process Methodology (OPM) diagram explaining the workflow of the algorithm. The document also explains the reasoning behind the choice of filters and parameters along with relevant examples.

For any questions please send an email to sreenivas.nm5493@gmail.com
