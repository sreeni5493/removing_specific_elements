\documentclass[a4paper,10pt]{article}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{float}
\usepackage{hyperref}
\usepackage{gensymb}
\usepackage{tabularx}
\usepackage{mathtools}
\hypersetup{
    colorlinks,
    citecolor=blue,
    filecolor=black,
    linkcolor=blue,
    urlcolor=blue
}
\usepackage{algorithm}
\usepackage{algpseudocode}

\usepackage[nottoc]{tocbibind} % for references to show up in table of contents

\usepackage{tikz}

\usepackage[linesnumbered,algo2e]{algorithm2e} % for algorithms
\begin{document}
%\selectlanguage{english}
%\pagenumbering{Roman}



\title{\begin{Huge}
\textbf{Assignment: Report}
\end{Huge}}
\author{\begin{Large}
Sreenivas Murali
\end{Large}}
\date{\today}
\maketitle \newpage
%\pagenumbering{arabic}
\section{Introduction}
This document describes the algorithm used for removing specific elements such as lines and contours in an given image when a specific pixel location is given as part of the line segment or contour.

\section{Algorithm choices}

The original idea was to detect lines in the image since the assignment specified that specific lines need to be removed. For this, the probabilistic hough transform was used \cite{houghp}. The hough algorithm converts the normal form of line $y = m x + c$ to a parametric form $\rho = x cos\theta + y sin\theta$ and bins each valid edge point in an image in a specific ($\rho, \theta$) space and based on a threshold, we determine which points belong to a line and which do not. The probabilistic hough transform is an extension of this algorithm, where only sample subset of points are taken in comparison to all edge points in an image. In order for us to use hough transform, we need to perform edge detection. While there are several edge detection operators such as sobel, prewitt and canny \cite{canny}, these gradient based operators are extra computations which are not required for this task. Also, in the case of canny, there is a smoothing operation at the start. This diffuses the pixel values over a neighborhood and leads to inaccurate edge measurements when accuracy is the main factor. In our context, there are darker text and symbols in brighter backgrounds. A simple binary thresholding is enough in the gray-scale image. The original sample image is shown in Figure \ref{original_image}.

\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.35]{../a1_1.PNG}
 \end{center}
 \caption{Original sample image}
 \label{original_image}
\end{figure}
The corresponding thresholded image is shown in Figure \ref{threshold_image}.
\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_thresholded_image.png}
 \end{center}
 \caption{Binary thresholded image with edges enhanced (brighter pixels represent an edge)}
 \label{threshold_image}
\end{figure}

We perform probabilistic hough transform to extracts all line segments in this image and retain the dominant ones are shown in Figure \ref{hough_output}.
\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_hough_lines.png}
 \end{center}
 \caption{Hough lines detected based on the binary image in Figure \ref{threshold_image}}
 \label{hough_output}
\end{figure}


Note that these lines are not enough if we wish to remove the triangle pointers at the end of these lines, since they can't be detected with hough transform. Also, one other disadvantage with using hough transform is the amount of parameters that needs to be optimized. This includes threshold parameter, amount of subdivisions for parametric transformations $\rho$ and $\theta$. In opencv's implementation of probabilistic hough transform, we also have two other parameters, minimum length for a line segment and maximum gap between consecutive lines.


Thus, we look for an alternate solution. For the alternate solution, we try to look at individual contours since the lines and triangles are connected together.  Contours can be found using the work by Suzuki et al. \cite{contours}. It uses the idea to locate neighboring binary pixels in a 4 or 8 pixel neighborhood and group them together as a single contour. This is implemented in \textit{findContour} function in opencv library. But we most note that, all the lines intersect in this application. Thus, a morphological erosion (removing brighter regions and reducing them) is required before finding the individual contours. A (2,2) kernel was used for erosion, since this is the least possible square kernel that is required to erode the connection between all the intersecting contours. After the erosion operation, we can perform contour detection. Also, we do not require all the contours. The biggest contour is the rectangle that encompasses all the text. This is the case in other images too. This can be removed and also the extremely smaller contours can also be removed. The result is shown in the below figure. (Figure \ref{contours1})

\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_contours.png}
 \end{center}
 \caption{Extracted contours}
 \label{contours1}
\end{figure}

From this, we can extract relevant contours based on pixel location. This is shown in Figure \ref{contours2}.

\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/contours_identified.png}
 \end{center}
 \caption{Relevant contours}
 \label{contours2}
\end{figure}

Finally, by using the pixel locations on the above image, we can decide to remove these segments in the original image (Figure \ref{original_image}). Due to the erosion operation that was performed, the result will not be correct since erosion removes parts of the line. (Figure \ref{result1})

\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_without_dilation.png}
 \end{center}
 \caption{Preliminary result with parts of the line segments removed}
 \label{result1}
\end{figure}

We can see that parts of the triangle are removed and also the line segment is thinner now. In order to do this properly, we need to dilate back (morphological dilation) the selected contours in Figure \ref{contours2} and then replace in the original image.

\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_with_dilation.png}
 \end{center}
 \caption{Result after dilation on Figure \ref{contours2} with (4,4) kernel}
 \label{result2}
 \end{figure}
 Note that, we can still see some minor noisy pixels in the triangles. In order to remove these a simple and non-complex solution is to smooth the output image with a filter operation. The choice used here is a median filter. The reason for choosing median filter over let's say Gaussian filter is that, median filter does not have a blurring effect. It just chooses the median value around a local neighborhood. This removes outliers. On the other hand, a Gaussian filter does averaging around a local neighborhood. This leads to blurry results as shown in the figure below.
 
 \begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_with_dilation_gaussian_blur.png}
 \end{center}
 \caption{Result with Gaussian blur}
 \label{result3}
\end{figure}
It can be noted that median blur does not have this effect and it produces sharper results.

 \begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.25]{images/result1_with_dilation_median_blur.png}
 \end{center}
 \caption{Result with median blur}
 \label{result4}
\end{figure}

\section{Work-flow}
The overall work-flow is shown in Figure \ref{work-flow} in an Object Process Methodology diagram (OPM). An OPM diagram is useful to describe systems where an object (rectangular block) represents data and process (elliptical block) represents an algorithm or process that is done on the object block (data). This is useful to describe a system work-flow. Alternate strategies include Unified Modeling Language (UML) diagrams. OPM is much more holistic since it gives equal importance to all its entities and we can clearly separate individual object-process blocks and its function.

\begin{figure}[H]
\captionsetup{justification=centering}
 \begin{center}
 \includegraphics[scale=0.5]{images/work-flow.png}
 \end{center}
 \caption{System work-flow using OPM}
 \label{work-flow}
\end{figure}


\section{Summary}
This document has clearly described the strategy used to find relevant line segments and removing them in a given image. In terms of future work, the algorithm can be optimized further for even more accuracy. Also, the optimization can done across a huge dataset, which will then ensure the parameters for the algorithms can be fixed to perform with maximum accuracy across a dataset.
\begin{thebibliography}{9}
\bibitem{houghp}
J. Matas, C. Galambos, and J. Kittler. \textit{"Progressive probabilistic Hough transform"}. In British Machine Vision Conference, pages 256-265, 1998.
\bibitem{canny}
Canny, J., \textit{"A Computational Approach To Edge Detection, IEEE Trans. Pattern Analysis and Machine Intelligence"}, 8(6):679–698, 1986.
\bibitem{contours}
Suzuki, S. and Abe, K., \textit{"Topological Structural Analysis of Digitized Binary Images by Border Following"} CVGIP 30 1, pp 32-46 (1985)

\end{thebibliography}
\end{document}

