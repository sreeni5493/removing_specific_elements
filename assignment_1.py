"""
.. module:: Assignment for removing specific elements such as lines in an image based on pixel location
   :platform: Unix
   :synopsis: This module takes a 3 channel input image and based on index locations provided (x and y), removes
   appropriate contours (lines or other edges).

.. module author:: Sreenivas Narasimha Murali (sreenivas.nm5493@gmail.com)

"""
import cv2
import numpy as np
from argparse import ArgumentParser


class Assignment:
    """
    This class deals with removing specific contours such as lines in an image based on user specifications,
    by giving pixel locations
    """
    def __init__(self, indices):
        """
        Initialization for the class
        Args:
            indices: type - list. X and Y location as tuple of lists with 2 elements in each
            individual tuple containing x and y indices. Ex: [[1, 2],[5, 6]].
            This will remove two contours in these two locations
        """
        self.indices = indices

    def __threshold_edges(self, im):
        """
        Finds the relevant edges in the image using a simple binary thresholding
        Args:
            im: type - N-D numpy array input 3 channel RGB image

        Returns:
            im_thresh: thresholded image enhancing edges
        """
        im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        ret, im_thresh = cv2.threshold(im_gray, 127, 255, cv2.THRESH_BINARY_INV)
        return im_thresh

    def __find_contours(self, im_thresh):
        """
        Takes a binary thresholded image with edges and returns a selected set of contours
        Args:
            im_thresh: Threshold image with edges enhances

        Returns:
            contours_selected: selected contours based on area of contours
        """
        # erosion is required to separate contours so that we can extract them individually
        kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (2, 2))
        im_thresh = cv2.erode(im_thresh, kernel, iterations=1)
        im_contour, contours, hierarchy = cv2.findContours(im_thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours_sorted = sorted(contours, key=lambda x: cv2.contourArea(x), reverse=True)
        # Select only relevant contours.
        # First contour contains the largest and all small contours at the end are removed
        contours_selected = contours_sorted[1:20]
        return contours_selected

    def __remove_selected_contours(self, im, contours_selected):
        """
        Remove contours based on given indices
        Args:
            im: Input 3 channel image
            contours_selected: Selected contours based on maximum contour area

        Returns:
            im_output: Output 3 channel image with specific contours removed
        """
        im_selected_contours = np.zeros(im.shape)
        number_contours = len(self.indices)
        for index in range(number_contours):
            for contour in contours_selected:
                if self.indices[index] in contour:
                    cv2.drawContours(im_selected_contours, [contour], 0, (255, 255, 255), -1)
        # dilation is required since we eroded the original contours
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))
        dilated_selected_contours = cv2.dilate(im_selected_contours, kernel, iterations=1)
        im[np.where(dilated_selected_contours > 0)] = 255

        im_output = cv2.medianBlur(im, 3)
        return im_output

    def remove_specific_elements(self, im):
        """
        This is the main function of the class that removes specific elements in an image
        based on user requirements
        Args:
            im: Input 3 channel image

        Returns:
            im_output: Output 3 channel image with specific elements removed
        """
        im_thresh = self.__threshold_edges(im)
        contours_selected = self.__find_contours(im_thresh)
        im_output = self.__remove_selected_contours(im, contours_selected)
        return im_output


# running the code: python assignment_1.py --indices "1,2,3,5" --image-location /path/to/image.png
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--indices',
                        help='delimited list input for indices. Example: "1,4,5,6,5,7". Parsed as'
                             'x, y locations in following list [[1,4],[5,6],[5,7]',
                        type=str, default="146,648,93,90")
    parser.add_argument('-loc', '--image-location',
                        help='image location',
                        type=str, default="a1_1.PNG")
    args = parser.parse_args()

    location_list = [int(item) for item in args.indices.split(',')]
    location_list = [location_list[i:i+2] for i in range(0, len(location_list), 2)]
    input_image = cv2.imread(args.image_location)
    remove_elements = Assignment(location_list)
    output_image = remove_elements.remove_specific_elements(input_image)
    cv2.imshow('Press any key to end program', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
